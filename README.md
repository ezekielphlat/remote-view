##About

Remote view is an application that would be used to view feeds from IP cameras of any type through webrtc api

##Task list

- [x] setup create-react-project
- [x] Create login React UI with material design
- [x] Create forgot password React UI with material design
- [x] Create reset password React UI with material design
- [x] Set up node js api server
- [x] Set up mongo db database schema for Devices and Users
- [x] Set up mongoose route for authentication
- [x] Add and setup redux on the react UI
- [x] Create Thunk action for userLog in activity and include axios api call.
- [x] Implement login and redirect to dashboard empty route.
- [x] Create Dashboard layout.
- [x] Implement Authorized route for user and guest in the react UI.
- [ ] Implement Forgot password
- [ ] Implement Reset Passord
- [ ] Create user registration form
- [ ] Create thunk action for use registration
- [ ] Implement user registration controller on the server side.
- [ ] Create UI for adding new device to the system
- [ ] Create thunk action to add new device to the system
- [ ] Implement add new device controller on the server side.
- [ ] Create UI to view remote feeds from device.
- [ ] Implement webrtc peer connection. to the raspberry pi devices added to the user.
- [ ] More tasks....
