import { USER_LOGGED_IN, USER_LOGGED_OUT } from "../types";
import api from "../api";

//action declared without dispatch.
export const userLoggedIn = user => ({
  type: USER_LOGGED_IN,
  user
});

//user logged out action with out dispatch and without payload
export const userLoggedOut = () => ({ type: USER_LOGGED_OUT });

//dispatch a login function
export const login = credentials => dispatch =>
  api.user.login(credentials).then(user => {
    localStorage.remoteViewJWT = user.token;

    dispatch(userLoggedIn(user));
  });
