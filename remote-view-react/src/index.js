import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import decode from "jwt-decode";
import { Provider } from "react-redux";
import { composeWithDevTools } from "redux-devtools-extension";
import App from "./App";
import "./css/main.css";
import rootReducer from "./rootReducer";
import * as serviceWorker from "./serviceWorker";
import { userLoggedIn } from "./actions/auth";

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

if (localStorage.remoteViewJWT) {
  const payload = decode(localStorage.remoteViewJWT);
  const user = {
    token: localStorage.remoteViewJWT,
    email: payload.email,
    role: payload.role,
    confirmed: payload.confirmed
  };
  store.dispatch(userLoggedIn(user));
}
ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <Route component={App} />
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
