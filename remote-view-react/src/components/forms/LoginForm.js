import React from "react";
import PropTypes from "prop-types";
import isEmail from "validator/lib/isEmail";
import { Link } from "react-router-dom";
import {
  Paper,
  Typography,
  TextField,
  Button,
  FormControl
} from "@material-ui/core";
import InlineError from "../messages/InlineError";

const styles = {
  form_paper: {
    padding: 10
  }
};

class LoginForm extends React.Component {
  state = {
    data: { email: "", password: "" },
    loading: false,
    errors: {}
  };
  onChange = e =>
    this.setState({
      ...this.state,
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  onSubmit = e => {
    e.preventDefault();

    const errors = this.validate(this.state.data);
    this.setState({ errors });

    if (Object.keys(errors).length === 0) {
      // this.setState({ loading: true });
      this.props
        .submit(this.state.data)
        .catch(err =>
          this.setState({ errors: err.response.data.errors, loading: false })
        );
    }
  };
  validate = data => {
    const errors = {};
    if (!data.password) errors.password = "Password field can't be blank";
    if (!isEmail(data.email))
      errors.email = "Please enter a valid email address";

    return errors;
  };
  render() {
    const { errors, data } = this.state;
    return (
      <Paper style={styles.form_paper}>
        <Typography variant="overline" align="center">
          Login
        </Typography>
        <form onSubmit={this.onSubmit}>
          {errors.global && (
            <Typography variant="h6">{errors.global}</Typography>
          )}
          <FormControl margin="normal" required fullWidth>
            <TextField
              id="outlined-email-input"
              label="Email"
              type="email"
              name="email"
              autoComplete="email"
              variant="outlined"
              fullWidth
              value={data.email}
              onChange={this.onChange}
              error={!!errors.email}
            />
            {errors.email && <InlineError text={errors.email} />}
          </FormControl>
          <br />
          <FormControl margin="normal" required fullWidth>
            <TextField
              id="outlined-password-input"
              label="Password"
              type="password"
              name="password"
              autoComplete="current-password"
              variant="outlined"
              fullWidth
              value={data.password}
              onChange={this.onChange}
              error={!!errors.password}
            />
            {errors.password && <InlineError text={errors.password} />}
          </FormControl>
          <Button type="submit" variant="contained" fullWidth color="primary">
            Login
          </Button>
        </form>
        <Typography>
          <Link to="/forgot_password">Forgot Password</Link>
        </Typography>
      </Paper>
    );
  }
}
LoginForm.propTypes = {
  submit: PropTypes.func.isRequired
};
export default LoginForm;
