import React from "react";
import PropTypes from "prop-types";

import {
  Paper,
  Typography,
  TextField,
  Button,
  FormControl
} from "@material-ui/core";

// import logo from "../../images/logo/logo.png";
// import InlineError from "../messages/InlineError";

const styles = {
  form_paper: {
    padding: 10
  }
};

class ForgotPasswordForm extends React.Component {
  state = {
    data: { email: "", password: "" },
    loading: false,
    errors: {}
  };
  onChange = e =>
    this.setState({
      ...this.state,
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props.submit(this.state.data);
      // .catch(err =>
      //   this.setState({ errors: err.response.data.errors, loading: false })
      // );
    }
  };
  validate = data => {
    const errors = {};
    return errors;
  };
  render() {
    // const { errors, data, loading } = this.state;
    return (
      <Paper style={styles.form_paper}>
        <Typography variant="overline" align="center">
          Forgot Password
        </Typography>
        <form>
          <FormControl margin="normal" required fullWidth>
            <TextField
              id="outlined-email-input"
              label="Enter Your Email"
              type="email"
              name="email"
              autoComplete="email"
              variant="outlined"
              fullWidth
            />
          </FormControl>

          <Button variant="contained" fullWidth color="primary">
            Send Confirmation Email
          </Button>
        </form>
      </Paper>
    );
  }
}
ForgotPasswordForm.propTypes = {
  submit: PropTypes.func.isRequired
};
export default ForgotPasswordForm;
