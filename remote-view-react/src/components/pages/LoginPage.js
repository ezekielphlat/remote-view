import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { login } from "../../actions/auth";
import LoginForm from "../../components/forms/LoginForm";
import { Grid, CssBaseline } from "@material-ui/core";
import LogoGrid from "../common/LogoGrid";

const styles = {
  grid: {
    height: "100%"
  }
};

class LoginPage extends React.Component {
  submit = data =>
    this.props.login(data).then(() => this.props.history.push("/dashboard"));
  render() {
    return (
      <Fragment>
        <CssBaseline />
        <Grid
          container
          style={styles.grid}
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={6} md={3}>
            <LogoGrid />
            <LoginForm submit={this.submit} />
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}
LoginPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  login: PropTypes.func.isRequired
};
export default connect(
  null,
  { login }
)(LoginPage);
