import React, { Fragment } from "react";

import {
  withStyles,
  CssBaseline,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Grid,
  Paper,
  Divider
} from "@material-ui/core";
import { Inbox as InboxIcon } from "@material-ui/icons";
import Header from "../layout/Header";
import SideBar from "../layout/SideBar";

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: 12
  },
  grid: {
    height: "100%"
  },
  leftPaper: {
    padding: 5,
    marginTop: 2,
    height: "100%"
  },
  rightPaper: {
    padding: 2,
    marginTop: 2,
    marginLeft: 2
  },
  active: {
    backgroundColor: theme.palette.primary.main,
    "& $primary, & $icon": {
      color: theme.palette.primary.light
    }
  },
  menuItem: {
    // "&:focus": {
    //   backgroundColor: theme.palette.primary.main,
    //   "& $primary, & $icon": {
    //     color: theme.palette.common.white
    //   }
    // }
  },
  primary: {},
  icon: {},
  avatar: {
    margin: 10,
    width: 80,
    height: 80
  }
});

class AdminDashboardLayout extends React.Component {
  state = {
    anchorEl: null,
    right: false
  };
  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open
    });
  };
  render() {
    const { classes } = this.props,
      { anchorEl } = this.state;

    const open = !!anchorEl;
    const sideList = (
      <div className={classes.list}>
        <List>
          <ListItem button>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="Profile" />
          </ListItem>

          <ListItem button>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="Active Devices" />
          </ListItem>
          <Divider />
          <ListItem button>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="Logout" />
          </ListItem>
        </List>
      </div>
    );

    return (
      <Fragment>
        <CssBaseline />
        <Header
          classes={classes}
          open={open}
          anchorEl={anchorEl}
          toggleDrawer={this.toggleDrawer}
          handleClose={this.handleClose}
        />

        <Grid container className={classes.grid}>
          <Grid item xs={12} sm={4} md={2}>
            <SideBar classes={classes} />
          </Grid>
          <Grid item xs={12} sm={8} md={10}>
            <Paper className={classes.rightPaper}>{this.props.children}</Paper>
          </Grid>
        </Grid>

        <Drawer
          anchor="right"
          open={this.state.right}
          onClose={this.toggleDrawer("right", false)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer("right", false)}
            onKeyDown={this.toggleDrawer("right", false)}
          >
            {sideList}
          </div>
        </Drawer>
      </Fragment>
    );
  }
}

export default withStyles(styles)(AdminDashboardLayout);
