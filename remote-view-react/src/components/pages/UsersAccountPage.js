import React, { Fragment } from "react";
import PropTypes from "prop-types";

import {
  Paper,
  Typography,
  Divider,
  AppBar,
  Tabs,
  Tab,
  withStyles
} from "@material-ui/core";
import AdminDashboardLayout from "./AdminDashboardLayout";
import PhoneIcon from "@material-ui/icons/Phone";
import FavoriteIcon from "@material-ui/icons/Favorite";
import PersonPinIcon from "@material-ui/icons/PersonPin";
import HelpIcon from "@material-ui/icons/Help";

import UserTable from "../tables/UserTable";

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  paper: {
    padding: 10
  }
});

class UsersAccountpage extends React.Component {
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };
  render() {
    const { classes } = this.props;
    const { value } = this.state;
    return (
      <Fragment>
        <AdminDashboardLayout>
          <Paper className={classes.paper}>
            <Typography variant="display1">Users Account</Typography>
            <Divider />

            <div className={classes.root}>
              <AppBar position="static" color="default">
                <Tabs
                  value={value}
                  onChange={this.handleChange}
                  variant="scrollable"
                  scrollButtons="on"
                  indicatorColor="primary"
                  textColor="primary"
                >
                  <Tab label="Search" icon={<PhoneIcon />} />
                  <Tab label="Users List" icon={<FavoriteIcon />} />
                  <Tab label="New User" icon={<FavoriteIcon />} />
                  <Tab label="Role List" icon={<PersonPinIcon />} />
                  <Tab label="New Role" icon={<HelpIcon />} />
                </Tabs>
              </AppBar>
              {value === 0 && <TabContainer>Search</TabContainer>}
              {value === 1 && (
                <TabContainer>
                  <UserTable />
                </TabContainer>
              )}
              {value === 2 && <TabContainer>New User</TabContainer>}
              {value === 3 && <TabContainer>Role List</TabContainer>}
              {value === 4 && <TabContainer>New Role</TabContainer>}
            </div>
          </Paper>
        </AdminDashboardLayout>
      </Fragment>
    );
  }
}

export default withStyles(styles)(UsersAccountpage);
