import React, { Fragment } from "react";
import { Paper, Typography, Grid, Divider } from "@material-ui/core";
import AdminDashboardLayout from "./AdminDashboardLayout";

const AdminDashboardPage = () => (
  <Fragment>
    <AdminDashboardLayout>
      <Paper>
        <Typography variant="display1">Dashboard</Typography>
        <Divider />
        <Grid container>
          <Grid item>
            <Typography variant="h6">Content here</Typography>
          </Grid>
        </Grid>
      </Paper>
    </AdminDashboardLayout>
  </Fragment>
);

export default AdminDashboardPage;
