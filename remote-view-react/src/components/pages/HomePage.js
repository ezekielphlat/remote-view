import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";

const HomePage = () => (
  <Fragment>
    <CssBaseline />
    <h1>Welcome to Iteco Remote View.</h1>
    <Link to="/login">Login</Link>
  </Fragment>
);

export default HomePage;
