import React, { Fragment } from "react";
import ForgotPasswordForm from "../../components/forms/ForgotPasswordForm";
import { Grid, CssBaseline } from "@material-ui/core";
import LogoGrid from "../common/LogoGrid";
const styles = {
  grid: {
    height: "100%"
  }
};

class ForgotPasswordPage extends React.Component {
  submit = data => {
    console.log("submited");
    window.location = "/dashboard";
  };

  render() {
    return (
      <Fragment>
        <CssBaseline />
        <Grid
          container
          style={styles.grid}
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={6} md={3}>
            <LogoGrid />
            <ForgotPasswordForm submit={this.submit} />
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}

export default ForgotPasswordPage;
