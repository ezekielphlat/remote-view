import React from "react";
import { Link } from "react-router-dom";
import {
  Paper,
  Grid,
  Divider,
  MenuList,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Avatar,
  Typography
} from "@material-ui/core";
import {
  Inbox as InboxIcon,
  Dashboard as DashboardIcon,
  People as PeopleIcon,
  Work as WorkIcon,
  VideoCall as VideoIcon
} from "@material-ui/icons";

const isActive = (value, classes) =>
  window.location.pathname === value ? classes.active : classes.MenuItem;

const SideBar = ({ classes }) => (
  <Paper className={classes.leftPaper}>
    <Grid container justify="center" alignItems="center">
      <Grid item>
        <Avatar className={classes.avatar}>A</Avatar>
      </Grid>
      <Grid item>
        <Typography variant="h6" align="center">
          welcome Admin
        </Typography>
      </Grid>
    </Grid>

    <Divider />
    <MenuList>
      <MenuItem
        component={Link}
        to="/dashboard"
        className={isActive("/dashboard", classes)}
      >
        <ListItemIcon className={classes.icon}>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText
          classes={{ primary: classes.primary }}
          inset
          primary="Dashboard"
        />
      </MenuItem>
      <MenuItem
        component={Link}
        to="/users_account"
        className={isActive("/users_account", classes)}
      >
        <ListItemIcon className={classes.icon}>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText
          classes={{ primary: classes.primary }}
          inset
          primary="Users Account"
        />
      </MenuItem>
      <MenuItem
        component={Link}
        to="/roles"
        className={isActive("/roles", classes)}
      >
        <ListItemIcon className={classes.icon}>
          <WorkIcon />
        </ListItemIcon>
        <ListItemText
          classes={{ primary: classes.primary }}
          inset
          primary="Roles"
        />
      </MenuItem>
      <MenuItem
        component={Link}
        to="/devices"
        className={isActive("/devices", classes)}
      >
        <ListItemIcon className={classes.icon}>
          <VideoIcon />
        </ListItemIcon>
        <ListItemText
          classes={{ primary: classes.primary }}
          inset
          primary="Devices"
        />
      </MenuItem>
      <MenuItem
        component={Link}
        to="/subscriptions"
        className={isActive("/subscriptions", classes)}
      >
        <ListItemIcon className={classes.icon}>
          <InboxIcon />
        </ListItemIcon>
        <ListItemText
          classes={{ primary: classes.primary }}
          inset
          primary="Subscriptions"
        />
      </MenuItem>
    </MenuList>
  </Paper>
);

export default SideBar;
