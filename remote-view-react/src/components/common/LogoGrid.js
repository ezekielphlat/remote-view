import React from "react";
import { Grid, Typography } from "@material-ui/core";
import logo from "../../images/logo/logo.png";
const LogoGrid = () => (
  <Grid container direction="column" justify="center" alignItems="center">
    <Grid item>
      <img alt="site logo" width="50" height="50" src={logo} />
    </Grid>
    <Grid item>
      <Typography variant="display1" gutterBottom align="center">
        Remote View
      </Typography>
    </Grid>
  </Grid>
);

export default LogoGrid;
