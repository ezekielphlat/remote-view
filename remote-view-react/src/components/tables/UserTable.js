import React from "react";

import MUIDataTable from "mui-datatables";

const columns = ["Name", "Device", "City", "State"];

const data = [
  ["Emeka Smith", "RBP/73638", "Victoria Island", "Lagos"],
  ["James Afariogun", "RBP/0948", "Oshodi", "Lagos"],
  ["Sola Johnson", "RBP/94848", "Oshogbo", "Osun"],
  ["James Ali", "RBP/7363", "Chibok", "Kano"]
];
const options = {
  filterType: "checkbox"
};
const UserTable = () => {
  return (
    <MUIDataTable
      title={"User List"}
      data={data}
      columns={columns}
      options={options}
    />
  );
};

export default UserTable;
