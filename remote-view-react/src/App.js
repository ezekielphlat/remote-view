import React from "react";

import PropTypes from "prop-types";

import HomePage from "./components/pages/HomePage";
import LoginPage from "./components/pages/LoginPage";
import ForgotPasswordPage from "./components/pages/ForgotPasswordPage";
import ResetPasswordPage from "./components/pages/ResetPasswordPage";
import AdminDashboardPage from "./components/pages/AdminDashboardPage";
import UsersAccountPage from "./components/pages/UsersAccountPage";
import RolesPage from "./components/pages/RolesPage";
import GuestRoute from "./components/routes/GuestRoute";
import UserRoute from "./components/routes/UserRoute";

const App = ({ location }) => (
  <div id="app">
    <style>{`html,
      body,
      body > div,body > div > #app  {
        height: 100%;
      }
    `}</style>

    <GuestRoute location={location} path="/" exact component={HomePage} />
    <GuestRoute location={location} path="/login" exact component={LoginPage} />

    <GuestRoute
      location={location}
      path="/forgot_password"
      exact
      component={ForgotPasswordPage}
    />
    <GuestRoute
      location={location}
      path="/reset_password"
      exact
      component={ResetPasswordPage}
    />
    <UserRoute
      location={location}
      path="/dashboard"
      exact
      component={AdminDashboardPage}
    />
    <UserRoute
      location={location}
      path="/users_account"
      exact
      component={UsersAccountPage}
    />
    <UserRoute location={location} path="/roles" exact component={RolesPage} />
  </div>
);
App.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
};
export default App;
