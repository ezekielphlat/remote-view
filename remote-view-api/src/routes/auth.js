import express from "express";
import bcrypt from "bcrypt";
import User from "../model/User";

const router = express.Router();

router.post("/", (req, res) => {
  const { credentials } = req.body;

  User.find({}).then(users => {
    if (Object.keys(users).length === 0) {
      User.insertMany([
        {
          email: "testadmin@test.com",
          passwordHash: bcrypt.hashSync("test", 10),
          role: "admin"
        },
        {
          email: "testManager@test.com",
          passwordHash: bcrypt.hashSync("test", 10),
          role: "manager"
        },
        {
          email: "testClient@test.com",
          passwordHash: bcrypt.hashSync("test", 10),
          role: "client"
        }
      ]);
    }
  });

  User.findOne({ email: credentials.email }).then(user => {
    if (user && user.isValidPassword(credentials.password)) {
      res.json({ user: user.toAuthJSON() });
    } else {
      res.status(400).json({ errors: { global: "invalid credentials" } });
    }
  });

  console.log(req.body);
});

export default router;
