import mongooose from "mongoose";

const schema = new mongooose.Schema(
  {
    name: { type: String, required: true, lowercase: true, unique: true },
    subCategoryId: {
      type: mongooose.Schema.Types.ObjectId,
      ref: "SubCategory"
    },
    active: { type: Boolean, required: true, default: true }
  },
  { timestamps: true }
);
export default mongooose.model("Subscription", schema);
