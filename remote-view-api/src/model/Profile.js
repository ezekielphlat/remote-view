import mongooose from "mongoose";

const schema = new mongooose.Schema(
  {
    firstName: { type: String, required: true, lowercase: true },
    lastName: { type: String, required: true, lowercase: true },
    identity: { type: mongooose.Schema.Types.ObjectId, ref: "User" },
    gender: { type: String, required: true, lowercase: true },
    homeAddress: { type: String, lowercase: true },
    City: { type: String, lowercase: true },
    Country: { type: String, lowercase: true },
    State: { type: String, lowercase: true },

    deviceId: { type: mongooose.Schema.Types.ObjectId, rel: "Device" },

    subscriptionId: {
      type: mongooose.Schema.Types.ObjectId,
      ref: "Subscription"
    },
    subscriptionActivationDate: Date,
    subscriptionExpiryDate: Date
  },
  { timestamps: true }
);
export default mongooose.model("Profile", schema);
