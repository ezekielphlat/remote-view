import mongooose from "mongoose";

const schema = new mongooose.Schema(
  {
    name: { type: String, required: true, lowercase: true }
  },
  { timestamps: true }
);
export default mongooose.model("Role", schema);
