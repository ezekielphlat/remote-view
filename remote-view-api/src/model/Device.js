import mongooose from "mongoose";

const schema = new mongooose.Schema(
  {
    name: { type: String, required: true, lowercase: true, unique: true },
    serialNumber: { type: String, required: true },
    model: { type: String, required: true },
    cameras: [{ type: String }]
  },
  { timestamps: true }
);
export default mongooose.model("Device", schema);
