import mongooose from "mongoose";

const schema = new mongooose.Schema(
  {
    name: { type: String, required: true, lowercase: true },
    active: { type: Boolean, required: true, default: true }
  },
  { timestamps: true }
);
export default mongooose.model("SubCategory", schema);
