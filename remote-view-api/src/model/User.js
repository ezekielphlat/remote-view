import mongoose from "mongoose";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import uniqueValidator from "mongoose-unique-validator";

const schema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      lowercase: true,
      index: true,
      unique: true
    },

    passwordHash: { type: String, required: true },
    // role: { type: mongoose.Schema.Types.ObjectId, ref: "Role" },
    role: { type: String, lowercase: true },
    confirmed: { type: Boolean, required: true, default: false },
    confirmationToken: { type: String, default: "" }
  },
  { timestamps: true } // adds createdAt and modifiedAt to the database
);
schema.methods.isValidPassword = function isValidPassword(password) {
  return bcrypt.compareSync(password, this.passwordHash);
};

schema.methods.generateJWT = function generateJWT() {
  return jwt.sign(
    {
      email: this.email,
      confirm: this.confirm,
      role: this.role
    },
    process.env.JWT_SECRET
  );
};

schema.methods.toAuthJSON = function toAuthJSON() {
  return {
    email: this.email,
    role: this.role,
    confirmed: this.confirm,
    token: this.generateJWT()
  };
};

schema.plugin(uniqueValidator, { message: "This email is already taken" });

export default mongoose.model("User", schema);
