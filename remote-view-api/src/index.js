import express from "express";
import path from "path";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import mongoose from "mongoose";
import auth from "./routes/auth";

dotenv.config();
const app = express();
app.use(bodyParser.json());
mongoose.connect(process.env.MONGODB_URL, {
  useNewUrlParser: true,
  useCreateIndex: true
});

app.use("/api/auth", auth);

app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.listen(8080, () => console.log("Running on port 8080"));
